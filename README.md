# Status AFS Security Review

## Offsite access to AFS@PSI

* authentication is required for offsite access to PSI's AFS cell
* further hardening by setting 'volume ACLs' is ongoing.
 *  volume ACLs set maximal rights for a volume. These ACLs cannot
    be overwritten by users.
 *  example: in user volumes (like the volume for `/afs/psi.ch/user/g/gsell`)
    only the owner `gsell` can change ACLs.
	
* authentication for offsite users with MFA
 * evaluation of options ongoing
  * MIT Kerberos supports MFA
    * most likely the best option
	* can be installed on AFS metadata servers (we already did in the past)
	* requires the move of the metadata servers to a special DMZ zone - 
	  which is already planed.
	* connecting AD and MIT Kerberos
        * either trust relation
        * or sync of user keys(passwords)
    * effort:
        * installation and configuration of MIT Kerberos or product like
          FreeIPA (Redhat, using MIT Kerberos): couple of days
        * operation: (based on passed experience) almost zero (software update)
        * connecting AD and MIT Kerberos: less then 1 month
          (main effort: learning how to set it up and testing) 
  * using PSI's ActiveDirectory?
    * as far as I know, AD doesn't support MFA (at least not as part of
      the Kerberos protocol)
    * if possible: effort would be minimal
  * Entra ID (Azure AD)
    * provides MFA but no straight forward method to obtain 
      Kerberos5 Ticket Granting Ticket
  * other options: Keycloak, ...
  
## LRT

* meeting with Hakim F.:
 * access to LRT volumes will be restricted to PSI nodes (including VPN)
 * this will be enforced by volume ACLs
 * AFS is not directly mentioned in the ISO certification, but several 
   AFS features are required/used. E.g.:
   * defined process to set user ACLs
   * volumes per activity
     * ACL
	 * quota
	 * recycling 
     * naming conventions for volumes (activities) with and without backup.
 * the possibility and effort to replace AFS is still unclear
 * the head of LRT is concerned 
   * about initial effort 
   * whether replacement solution can be operated with same effort
 * why should AFS be replaced at all? For LRT AFS is one if the 
   most stable services provided by AIT/AWI.
 
## Pmodules on NFS

* Issues with NFS, system reboot was required
